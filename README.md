This project includes custom select input which should totally repeat behaviour of native select on desktop

---

### Before start install dependencies

----

Run this command in root directory
```
yarn install
```

----

### For review common select state

Run this command in root of the project

```
yarn run storybook
```

go to url 

```
http://localhost:9009/?path=/story/select--with-label
```

----

### Available Scripts

---

##### Run components storybook for review on local

```
yarn run storybook
```

##### Build components storybook for deploy it to server

```
yarn run build-storybook
```

Static files will be available in `build/storybook` directory


##### Lint commands for check code style

Run styles and javascript linters

```
 yarn run lint
```

Run styles linter

```
 yarn run lint:css
```

Run javascript linter

```
 yarn run lint:eslint
```

##### Test commands

Run js test

```
yarn run test
```
