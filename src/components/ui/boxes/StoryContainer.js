import React, {} from 'react';
import * as PropTypes from 'prop-types';

const StoryContainer = (
  { children },
) => (
  <div style={{ padding: 40 }}>
    {children}
  </div>
);

StoryContainer.propTypes = { children: PropTypes.node.isRequired };

StoryContainer.defaultProps = {};

export default StoryContainer;
