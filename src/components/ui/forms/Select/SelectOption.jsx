import React from 'react';
import classNames from 'classnames';
import * as PropTypes from 'prop-types';

import Checkbox from '../Checkbox/Checkbox';
import classes from './Select.module.scss';

const SelectOption = (
  {
    isSelected,
    label,
    value,
    hasDivider,
    isCheckbox,
    ...rest
  },
) => {
  const itemProperties = {
    'data-value': value,
    tabIndex: -1,
  };
  const className = classNames(
    classes.option,
    { [classes.divider]: hasDivider },
  );
  return (isCheckbox
    ? (
      <Checkbox
        {...itemProperties}
        className={className}
        readOnly
        label={label}
        name='option'
        checked={isSelected}
      />
    )
    : (
      <div
        {...rest}
        {...itemProperties}
        className={
          classNames(
            className,
            { [classes.selected]: isSelected },
          )
        }
      >
        {label}
      </div>
    ));
};

SelectOption.propTypes = {
  /**
   * Text
   */
  label: PropTypes.node.isRequired,
  /**
   * Value key
   */
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  /**
   * is option selected
   */
  isSelected: PropTypes.bool,
  /**
   * is option has divider, bottom border
   */
  hasDivider: PropTypes.bool,
  /**
   * Should be represented as checkbox
   */
  isCheckbox: PropTypes.bool,
};

SelectOption.defaultProps = {
  isSelected: false,
  hasDivider: true,
  isCheckbox: false,
};

export default SelectOption;
