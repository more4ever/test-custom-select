import React from 'react';
import classNames from 'classnames';
import * as PropTypes from 'prop-types';

import DropDownMenu from '../../DropDownMenu/DropDownMenu';
import DropDownIcon from '../Icons/DropDownIcon';
import Label from '../Label/Label';
import SelectOption from './SelectOption';
import classes from './Select.module.scss';

const keyIds = {
  up: 'up',
  down: 'down',
  esc: 'esc',
  tab: 'tab',
  space: 'space',
  enter: 'enter',
};

const keyCodeToId = {
  38: keyIds.up,
  40: keyIds.down,
  27: keyIds.esc,
  9: keyIds.tab,
  32: keyIds.space,
  13: keyIds.enter,
};

const defaultPlaceholder = 'Please select...';
const initialState = { isMenuOpen: false };

const getEventKeyId = ({ keyCode, which, charCode }) => {
  const code = keyCode || which || charCode;

  return keyCodeToId[code];
};

class Select extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = { ...initialState };
    this.menuRef = undefined;
    this.handleRef = undefined;
  }

  componentDidMount() {
    document.addEventListener('click', this.closeMenu);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.closeMenu);
  }

  setMenuRef = (ref) => {
    this.menuRef = ref;
  };

  setHandlerRef = (ref) => {
    this.handleRef = ref;
  };
  /**
   * Generate handler text. It can be placeholder or joined labels string for selected items
   * @return {string}
   */

  getHandlerText() {
    const { placeholder = defaultPlaceholder, selected, isMultiple, options } = this.props;
    const isSelectedArray = Boolean(Array.isArray(selected) && selected.length);

    if (
      !selected || (isMultiple && !isSelectedArray)
    ) {
      return placeholder;
    }
    return options
      .reduce(
        (accum, { value, label }) => {
          if (isSelectedArray ? selected.includes(value) : selected === value) {
            return [...accum, label];
          }

          return accum;
        },
        [],
      )
      .join(', ');
  }

  stopPropagation = (event) => {
    event.stopPropagation();
    event.nativeEvent.stopImmediatePropagation();
  };

  setIsOpenState = (value, callback) => {
    const { disabled } = this.props;

    if (disabled) {
      return;
    }

    this.setState({ isMenuOpen: value }, callback);
  };

  toggleMenu = (event) => {
    this.stopPropagation(event);
    const { isMenuOpen } = this.state;
    this.setIsOpenState(!isMenuOpen);
  };

  openMenu = (callback) => {
    this.setIsOpenState(true, callback);
  };

  closeMenu = () => {
    const { isMenuOpen } = this.state;

    if (!isMenuOpen) {
      return;
    }

    this.setIsOpenState(false);
    this.handleRef.focus();
  };

  /**
   * Trigger dropdown open and move focus to first element in list
   */
  startMenuNavigation = () => {
    this.openMenu(() => {
      if (this.menuRef && this.menuRef.firstChild) {
        const { firstChild } = this.menuRef;
        firstChild.focus();
      }
    });
  };
  /**
   * Handle keydown keyboard on select handler for open dropdown:
   * @param event
   */

  onKeyDown = (event) => {
    const keyId = getEventKeyId(event);

    if (!keyId) {
      return;
    }

    const { isMenuOpen } = this.state;

    switch (keyId) {
      case keyIds.up:
      case keyIds.down:
        this.startMenuNavigation();
        break;
      case keyIds.tab:
        if (isMenuOpen) {
          event.preventDefault();
        }
        break;
      default:
        break;
    }
  };
  /**
   * Handle onClick and keyDown for space and enter
   * @param target
   */

  onItemAction = ({ target }) => {
    const value = target.getAttribute('data-value');
    const { onChange, isMultiple, selected } = this.props;
    if (!value) {
      return;
    }
    if (isMultiple) {
      if (!Array.isArray(selected)) {
        onChange([value]);
        return;
      }
      if (selected.includes(value)) {
        onChange(selected.filter((selectedValue) => selectedValue !== value));
      } else {
        onChange([...selected, value]);
      }
    } else {
      onChange(value);
    }

    if (!isMultiple) {
      this.closeMenu();
    }
  };
  /**
   * Handle navigation through options list using arrows
   * @param nextSibling
   * @param previousSibling
   * @param keyId
   */

  onListNavigate = ({ target: { nextSibling, previousSibling } }, keyId) => {
    if (keyId === keyIds.down && nextSibling) {
      nextSibling.focus();
    } else if (keyId === keyIds.up && previousSibling) {
      previousSibling.focus();
    }
  };
  /**
   * Handle user key down for bind keyboard actions for dropdown items
   * @param {Event} event
   */

  onItemKeyDown = (event) => {
    const keyId = getEventKeyId(event);

    if (!keyId) {
      return;
    }
    event.preventDefault();
    switch (keyId) {
      case keyIds.space:
      case keyIds.enter:
        this.onItemAction(event);
        break;
      case keyIds.up:
      case keyIds.down:
        this.onListNavigate(event, keyId);
        break;
      case keyIds.esc:
        if (this.handleRef) {
          this.handleRef.focus();
        }
        this.closeMenu();
        break;
      default:
        break;
    }
  };

  hasSelectedValue() {
    const { selected, isMultiple } = this.props;

    return Boolean(
      (isMultiple && Array.isArray(selected) && selected.length)
    || selected,
    );
  }

  render() {
    const {
      className,
      disabled = false,
      options,
      name,
      selected,
      label: selectInput,
      tabIndex = 0,
      isMultiple,
    } = this.props;

    const { isMenuOpen } = this.state;
    const lastIndex = options.length - 1;
    const hasSelectedValue = this.hasSelectedValue();

    return (
      <div>
        {
          selectInput && (
            <Label title={selectInput} htmlFor={name} tabIndex={-1} />
          )
        }
        <div
          className={
            classNames(
              classes.root,
              className,
              {
                [classes.disabled]: disabled,
                [classes.open]: isMenuOpen,
              },
            )
          }
          onClick={this.stopPropagation}
          role='presentation'
        >
          <div
            id={name}
            ref={this.setHandlerRef}
            className={classNames(classes.handler, { [classes.hasSelected]: hasSelectedValue })}
            onClick={this.toggleMenu}
            onKeyDown={this.onKeyDown}
            role='button'
            tabIndex={disabled ? -1 : tabIndex}
          >
            <div className={classes.text}>
              {this.getHandlerText()}
            </div>
            <DropDownIcon className={classes.caret} />
          </div>
          <DropDownMenu
            isOpen={isMenuOpen}
            className={classes.menu}
            onClick={this.onItemAction}
            onKeyDown={this.onItemKeyDown}
            ref={this.setMenuRef}
          >
            {
              options
                .map(
                  ({ label, value }, index) => {
                    const isSelected = Array.isArray(selected)
                      ? selected.includes(value)
                      : value === selected;
                    return (
                      <SelectOption
                        key={`${name}-option-${value}`}
                        isSelected={isSelected}
                        label={label}
                        value={value}
                        hasDivider={index !== lastIndex}
                        isCheckbox={isMultiple}
                      />
                    );
                  },
                )
            }
          </DropDownMenu>
        </div>
      </div>
    );
  }
}

const valueType = PropTypes.oneOfType([PropTypes.string, PropTypes.number]);

const optionShape = PropTypes.shape({
  /**
   * Will be rendered as UI part
   */
  label: PropTypes.node,
  /**
   * Will be used as value identification for determinate selected state
   */
  value: valueType.isRequired,
});

Select.propTypes = {
  /**
   * Array of options for render list
   */
  options: PropTypes.arrayOf(optionShape).isRequired,
  /**
 * Array of selected values or single selected value
 */
  selected: PropTypes.oneOfType([PropTypes.arrayOf(valueType), valueType]).isRequired,
  /**
 * Input name for onChange event implementation
 */
  name: PropTypes.string.isRequired,
  /**
 * For control focus order with tab
 */
  onChange: PropTypes.func.isRequired,
  /**
 * Display label
 */
  label: PropTypes.node,
  /**
 * For control focus order with tab
 */
  tabIndex: PropTypes.number,
  /**
 * Css class for root element
 */
  className: PropTypes.string,
  /**
 * Handler text when no value selected
 */
  placeholder: PropTypes.string,
  /**
 * Is input disabled
 */
  disabled: PropTypes.bool,
  /**
 * Is input multiple
 */
  isMultiple: PropTypes.bool,
};

Select.defaultProps = {
  label: undefined,
  tabIndex: 0,
  className: undefined,
  placeholder: undefined,
  disabled: false,
  isMultiple: false,
};

export default Select;
