import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';

import Select from './Select';

const name = 'subscription';

const firstOption = {
  label: 'Subscription 1',
  value: 'subscription1',
};
const secondOption = {
  label: 'Subscription 2',
  value: 'subscription2',
};
const thirdOption = {
  label: 'Subscription 3',
  value: 'subscription3',
};

const options = [
  firstOption,
  secondOption,
  thirdOption,
];

const dummyPlaceholder = 'Placeholder lorem ipsum...';
const dummyLabel = 'Label lorem ipsum...';

describe(
  'Select',
  () => {
    afterEach(cleanup);

    it('Placeholder renders', () => {
      const { getByText } = render(<Select
        options={[]}
        selected=''
        name={name}
        onChange={() => {
        }}
        placeholder={dummyPlaceholder}
      />);

      const element = getByText(dummyPlaceholder);

      expect(element).toBeInTheDocument();
    });

    it('Label renders', () => {
      const { getByText } = render(<Select
        options={[]}
        selected=''
        name={name}
        onChange={() => {
        }}
        label={dummyLabel}
      />);

      const element = getByText(dummyLabel);

      expect(element).toBeInTheDocument();
    });

    it('Options renders', () => {
      const { getByText } = render(<Select
        options={options}
        selected=''
        name={name}
        onChange={() => {
        }}
        label={dummyLabel}
      />);

      const firstOptionElement = getByText(firstOption.label);

      expect(firstOptionElement).toBeInTheDocument();
    });

    it('Option has correct value attribute', () => {
      const { getByText } = render(<Select
        options={options}
        selected=''
        name={name}
        onChange={() => {
        }}
        label={dummyLabel}
      />);

      const firstOptionElement = getByText(firstOption.label);

      expect(firstOptionElement.getAttribute('data-value'))
        .toEqual(firstOption.value);
    });

    it('On change fired correctly for single select', () => {
      const onChange = jest.fn();
      const { getByText } = render(<Select
        options={options}
        selected=''
        name={name}
        onChange={onChange}
        label={dummyLabel}
      />);

      const firstOptionElement = getByText(firstOption.label);
      fireEvent.click(firstOptionElement);
      expect(onChange).toHaveBeenCalled();
      expect(onChange).toHaveBeenCalledWith(firstOption.value);
    });

    it('Placeholder replaced correctly with selected label', () => {
      const { getByRole } = render(<Select
        options={options}
        selected={secondOption.value}
        name={name}
        onChange={() => {
        }}
        label={dummyLabel}
      />);

      const handlerElement = getByRole('button');

      expect(handlerElement.textContent).toEqual(secondOption.label);
    });

    it('On change fired correctly for multiple select', () => {
      const onChange = jest.fn();
      const { getByRole } = render(<Select
        isMultiple
        options={options}
        selected={[]}
        name={name}
        onChange={onChange}
        label={dummyLabel}
      />);

      const dialogElement = getByRole('listbox');
      expect(dialogElement).toBeInTheDocument();

      expect(dialogElement.childNodes.length).toEqual(options.length);

      const firstOptionElement = dialogElement.childNodes[0];

      expect(firstOptionElement).toBeInTheDocument();
      fireEvent.click(firstOptionElement);

      expect(onChange).toHaveBeenCalledTimes(1);
      expect(onChange).toHaveBeenCalledWith([firstOption.value]);
    });
  },
);
