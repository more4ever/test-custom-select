import React, { useCallback, useState } from 'react';
import { action } from '@storybook/addon-actions';

import StoryContainer from '../../boxes/StoryContainer';
import Select from './Select';

export default {
  component: Select,
  title: 'Select',
};

const name = 'subscription';

const options = [
  {
    label: 'Subscription 1',
    value: 'subscription1',
  },
  {
    label: 'Subscription 2',
    value: 'subscription2',
  },
  {
    label: 'Subscription 3',
    value: 'subscription3',
  },
];

const storyOnChange = action('changed');

export const withLabel = () => {
  const [selected, setSelected] = useState('');
  const onChange = useCallback(
    (nextValue) => {
      setSelected(nextValue);
      storyOnChange(nextValue);
    },
    [setSelected],
  );

  return (
    <StoryContainer>
      <Select
        name={name}
        selected={selected}
        label='Test label'
        options={options}
        placeholder='placeholder'
        onChange={onChange}
      />
    </StoryContainer>
  );
};

export const withoutPlaceholder = () => {
  const [selected, setSelected] = useState('');
  const onChange = useCallback(
    (nextValue) => {
      setSelected(nextValue);
      storyOnChange(nextValue);
    },
    [setSelected],
  );

  return (
    <StoryContainer>
      <Select
        name={name}
        selected={selected}
        label='Test label'
        options={options}
        onChange={onChange}
      />
    </StoryContainer>
  );
};

export const multipleSelected = () => {
  const [selected, setSelected] = useState('');
  const onChange = useCallback(
    (nextValue) => {
      setSelected(nextValue);
      storyOnChange(nextValue);
    },
    [setSelected],
  );

  return (
    <StoryContainer>
      <Select
        name={name}
        label='Test label'
        isMultiple
        options={options}
        selected={selected}
        onChange={onChange}
      />
    </StoryContainer>
  );
};

export const disabled = () => {
  const [selected, setSelected] = useState('');
  const onChange = useCallback(
    (nextValue) => {
      setSelected(nextValue);
      storyOnChange(nextValue);
    },
    [setSelected],
  );

  return (
    <StoryContainer>
      <Select
        name={name}
        label='Test label'
        options={options}
        disabled
        selected={selected}
        onChange={onChange}
      />
    </StoryContainer>
  );
};
