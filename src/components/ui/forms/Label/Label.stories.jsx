/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';

import Label from './Label';

export default {
  component: Label,
  title: 'Label',
};

export const normal = () => (
  <Label title='Hello Label' />
);

export const withInput = () => (
  <Label title='Firstname *'>
    <input name='firstname' />
  </Label>
);

export const toTheRight = () => (
  <Label position='right' title='Firstname *'>
    <input name='firstname' />
  </Label>
);

export const disabled = () => (
  <Label title='Firstname *' disabled>
    <input name='firstname' disabled />
  </Label>
);
