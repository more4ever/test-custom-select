import React, { forwardRef } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Label.module.scss';

const Label = forwardRef((props, ref) => {
  const {
    children, title, position, disabled, tabIndex,
    onKeyPress, className, onKeyDown, htmlFor, ...rest
  } = props;

  const text = (
    <div className={styles.label}>
      {title}
    </div>
  );
  const classes = classNames(
    styles.container,
    className,
    { [styles.right]: position === 'right', [styles.disabled]: disabled },
  );

  return (
    <label
      className={classes}
      ref={ref}
      tabIndex={tabIndex}
      onKeyPress={onKeyPress}
      onKeyDown={onKeyDown}
      role='presentation'
      htmlFor={htmlFor}
      {...rest}
    >
      {text}
      {children}
    </label>
  );
});

Label.propTypes = {
  children: PropTypes.node,
  /**
   * Displayed title
   */
  title: PropTypes.string.isRequired,

  /**
   * Position of label relative to children (above child, to the right of child, ...)
   */
  position: PropTypes.oneOf(['above', 'right']),

  /**
   * Read only mode. Default: false
   */
  disabled: PropTypes.bool,

  /**
   * tab index
   */
  tabIndex: PropTypes.number,

  /**
   * label className
   */
  className: PropTypes.string,
  /**
   * for attribute
   */
  htmlFor: PropTypes.string,

  /**
   * Handle keyboard events
   */
  onKeyPress: PropTypes.func,
  /**
   * Handle keyboard events
   */
  onKeyDown: PropTypes.func,
};

Label.defaultProps = {
  children: null,
  position: 'above',
  disabled: false,
  tabIndex: 0,
  className: undefined,
  htmlFor: undefined,
  onKeyPress: undefined,
  onKeyDown: undefined,
};

export default Label;
