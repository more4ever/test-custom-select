import React from 'react';
import { action } from '@storybook/addon-actions';

import Checkbox from './Checkbox';
import StoryContainer from '../../boxes/StoryContainer';

export default {
  component: Checkbox,
  title: 'Checkbox',
};

const name = 'checkbox';

export const normal = () => (
  <StoryContainer>
    <Checkbox
      name={name}
      label='Checkbox'
      onChange={action('changed')}
    />
  </StoryContainer>
);

export const disabled = () => (
  <StoryContainer>
    <Checkbox
      name={name}
      label='Checkbox'
      disabled
      onChange={action('changed')}
    />
  </StoryContainer>
);
