import React, { forwardRef, useCallback } from 'react';
import classNames from 'classnames';
import * as PropTypes from 'prop-types';

import Label from '../Label/Label';
import classes from './Checkbox.module.scss';
import CheckboxCheckMark from '../Icons/CheckboxCheckMark';

const Checkbox = forwardRef(
  (
    {
      label, name, children, tabIndex = 0,
      labelPosition, disabled,
      className, checked, onChange, readOnly,
      onKeyDown, 'data-value': dataValue,
    },
    ref,
  ) => {
    const onSpacePress = useCallback(
      ({ keyCode, charCode, which, currentTarget }) => {
        if (disabled) {
          return;
        }

        if ([charCode, keyCode, which].includes(32)) {
          currentTarget.click();
        }
      },
      [disabled],
    );

    return (
      <Label
        title={label}
        className={classNames(classes.root, className, { [classes.disabled]: disabled })}
        tabIndex={tabIndex}
        position={labelPosition}
        onKeyPress={onSpacePress}
        onKeyDown={onKeyDown}
        disabled={disabled}
        data-value={dataValue}
      >
        <input
          name={name}
          type='checkbox'
          className={classes.nativeInput}
          ref={ref}
          tabIndex={-1}
          disabled={disabled}
          checked={checked}
          onChange={onChange}
          readOnly={readOnly}
        />
        <div
          className={classNames(classes.customCheckbox)}
        >
          <CheckboxCheckMark className={classes.checkMark} />
        </div>

        {children}
      </Label>
    );
  },
);

Checkbox.propTypes = {
  children: PropTypes.node,
  /**
   * Displayed label
   */
  label: PropTypes.node.isRequired,
  /**
   *input name for connect with form
   */
  name: PropTypes.string.isRequired,
  /**
   * Position of label relative to children (above child, to the right of child, ...)
   */
  labelPosition: PropTypes.string,
  /**
   * Is input checked or not
   */
  checked: PropTypes.bool,
  /**
   * For controlled checkbox
   */
  onChange: PropTypes.func,
  /**
   * Read only mode. Default: false
   */
  disabled: PropTypes.bool,
  /**
   * For case when checkbox can't be changed directly
   */
  readOnly: PropTypes.bool,
  /**
   * custom tab index for control navigation with `tab` order
   */
  tabIndex: PropTypes.number,
  /**
   * label className
   */
  className: PropTypes.string,
  /**
   * On key down
   */
  onKeyDown: PropTypes.func,
  /**
   * custom attribute for simple usage in select
   */
  'data-value': PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Checkbox.defaultProps = {
  children: null,
  labelPosition: 'right',
  checked: undefined,
  onChange: undefined,
  disabled: false,
  readOnly: false,
  tabIndex: 0,
  className: undefined,
  onKeyDown: undefined,
  'data-value': undefined,
};

export default Checkbox;
