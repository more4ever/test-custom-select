import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';

import Checkbox from './Checkbox';

const label = 'label';
const name = 'checkbox';

describe(
  'Checkbox',
  () => {
    afterEach(cleanup);

    it('renders label', () => {
      const { getByText } = render(
        <Checkbox
          label={label}
          name={name}
        />,
      );

      const element = getByText(label);
      expect(element).toBeInTheDocument();
    });

    it('Click to label trigger input change', () => {
      const { getByText, getByLabelText } = render(
        <Checkbox
          label={label}
          name={name}
        />,
      );

      const labelElement = getByText(label).closest('label');

      expect(labelElement).toBeInTheDocument();

      const inputElement = getByLabelText(label);

      fireEvent.click(labelElement);

      expect(inputElement.checked).toEqual(true);
    });

    it('Controlled checkbox behaviour, onChange event', () => {
      const handleChange = jest.fn();
      const { getByText } = render(
        <Checkbox
          label={label}
          name={name}
          onChange={handleChange}
          checked={false}
        />,
      );

      const labelElement = getByText(label).closest('label');

      expect(labelElement).toBeInTheDocument();

      fireEvent.click(labelElement);

      expect(handleChange).toHaveBeenCalled();
    });

    it('Disabled prop prevents input toggle', () => {
      const handleChange = jest.fn();
      const { getByText, getByLabelText } = render(
        <Checkbox
          label={label}
          name={name}
          onChange={handleChange}
          checked={false}
        />,
      );

      const labelElement = getByText(label).closest('label');

      expect(labelElement).toBeInTheDocument();

      const inputElement = getByLabelText(label);

      fireEvent.click(labelElement);

      expect(inputElement.checked).toEqual(false);
    });
  },
);
