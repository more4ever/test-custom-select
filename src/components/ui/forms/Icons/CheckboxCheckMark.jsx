import React from 'react';
import * as PropTypes from 'prop-types';

const CheckboxCheckMark = ({ className }) => (
  <svg
    width='11'
    height='9'
    className={className}
    viewBox='0 0 11 9'
  >
    <path
      d='M11 1.84819L4.12561 8.51501L0 4.51454L1.37488 3.18137L4.12561 5.84866L9.62512 0.515015L11 1.84819Z'
      fill='white'
    />
  </svg>
);

CheckboxCheckMark.propTypes = { className: PropTypes.string };

CheckboxCheckMark.defaultProps = { className: undefined };

export default CheckboxCheckMark;
