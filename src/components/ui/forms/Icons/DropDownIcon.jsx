import * as PropTypes from 'prop-types';
import React from 'react';

const DropDownIcon = ({ className }) => (
  <svg
    width='8'
    height='4'
    xmlns='http://www.w3.org/2000/svg'
    className={className}
    viewBox='0 0 8 4'
  >
    <path d='M4 4L0 0H8L4 4Z' />
  </svg>
);

DropDownIcon.propTypes = { className: PropTypes.string };

DropDownIcon.defaultProps = { className: undefined };

export default DropDownIcon;
