import React from 'react';
import classNames from 'classnames';
import * as PropTypes from 'prop-types';

import classes from './DropDownMenu.module.scss';

const DropDownMenu = React.forwardRef(
  (
    {
      isOpen,
      className,
      children,
      onClick,
      onKeyDown,
      tabIndex,
    },
    ref,
  ) => (
    <div
      onClick={onClick}
      onKeyDown={onKeyDown}
      role='listbox'
      ref={ref}
      tabIndex={tabIndex}
      className={
        classNames(
          classes.root,
          className,
          { [classes.open]: isOpen },
        )
      }
    >
      {children}
    </div>
  ),
);

DropDownMenu.propTypes = {
  children: PropTypes.node.isRequired,
  /**
   * Control on Menu visibility
   */
  isOpen: PropTypes.bool.isRequired,
  /**
   * Custom class name
   */
  className: PropTypes.string,
  /**
   * For focus move
   */
  tabIndex: PropTypes.number,
  /**
   * Event handler
   */
  onClick: PropTypes.func,
  /**
   * Event handler
   */
  onKeyDown: PropTypes.func,
};

DropDownMenu.defaultProps = {
  className: undefined,
  tabIndex: -1,
  onClick: undefined,
  onKeyDown: undefined,
};

export default DropDownMenu;
