import React from 'react';

import StoryContainer from '../boxes/StoryContainer';
import DropDownMenu from './DropDownMenu';

export default {
  component: DropDownMenu,
  title: 'Drop Down Menu',
};

export const normal = () => (
  <StoryContainer>
    <DropDownMenu isOpen>
      <div style={{ padding: 20 }}>
        Test content of menu
      </div>
    </DropDownMenu>
  </StoryContainer>
);
