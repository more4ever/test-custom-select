import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';

import DropDownMenu from './DropDownMenu';

const dummyText = 'Lorem ipsum dolor set';

describe(
  'Drop Down Menu',
  () => {
    afterEach(cleanup);

    it('renders in document with isOpen=`true`', () => {
      const { getByText } = render(
        <DropDownMenu isOpen>
          {dummyText}
        </DropDownMenu>,
      );

      const element = getByText(dummyText);
      expect(element).toBeInTheDocument();
    });

    it('element has dialog role', () => {
      const { getByRole } = render(
        <DropDownMenu isOpen>
          {dummyText}
        </DropDownMenu>,
      );

      const element = getByRole('listbox');
      expect(element).toBeInTheDocument();
    });

    it('renders in document with isOpen=`false`', () => {
      const { getByText } = render(
        <DropDownMenu isOpen={false}>
          {dummyText}
        </DropDownMenu>,
      );

      const element = getByText(dummyText);
      expect(element).toBeInTheDocument();
    });

    it('onClick event successfully fired', () => {
      const handleClick = jest.fn();
      const { getByText } = render(
        <DropDownMenu
          isOpen
          onClick={handleClick}
        >
          {dummyText}
        </DropDownMenu>,
      );
      const element = getByText(dummyText);

      fireEvent.click(element);

      expect(handleClick).toHaveBeenCalled();
    });
  },
);
