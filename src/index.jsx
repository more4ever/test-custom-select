import React from 'react';
import ReactDOM from 'react-dom';

class Root extends React.Component {
  render() {
    return 'All components available in storybook';
  }
}

ReactDOM.render(
  <Root />,
  document.getElementById('root'),
);
